import "../styles/todo.css";
import { useState, useEffect } from "react";
function Todo() {
  const [todo, setTodo] = useState([]);
  const [newTodo, setNewTodo] = useState([])
  const [checked, setChecked] = useState(false);
//   const handleClick = () => {
//     setChecked(!checked);
//     console.log(!checked);
//   };
    function handle(id){
        console.log(id)
        todo.splice(id,1)
        setNewTodo(newTodo.concat(...todo))
        // var newTodos = todo
        // setTodo(newTodos)
        console.log(newTodo)
      
    }
   
  function TodoArray(e) {
      
    if (e.key === "Enter" && e.target.value.length !== 0) {
        
      setTodo(todo.concat(e.target.value));
      e.target.value = "";
    }
  }

  return (
    <div className="Todo">
      <div className="header">
        <h1>Todo Planner</h1>
      </div>
      <div className="Input">
        <input
          className="todoInput"
          placeholder="Add a todo and press enter to add..."
          onChange={e => TodoArray(e)}
          onKeyPress={event => {
            TodoArray(event);
          }}
        />
      </div>
      <hr />
      {(todo.length !== 0)
        ? todo.map((item, id) => {
            console.log(todo.length);
            return (
              <div className="Input-box" key={id} style={{ display: "flex", justifyContent: "space-between" }}>
                
                <div>
                  {item}
                </div>
                <button
                  
                  onClick={()=>{
                      handle(id)
                  }}
                //   checked={checked}
                  style={{ float: "right", backgroundColor: "green", color: "whitesmoke"}}
                >Done</button>
              </div>
            );
          })
        : (<div><p style={{color: "gray"}}>No todo's added :(</p></div>)}
    </div>
  );
}

export default Todo;
